#include "Reg.h"

/*
 ================================================================
 ========================== PINES ===============================
 ================================================================

 ------------------------PWM-------------------------

 P1.18= PWM1.1  (pin PAD1) - P1.20= PWM1.2  (pin PAD3 )
 motor L: P0.6=marcha atras PWM1.2 (pin 8) - P0.7= marcha adelante PWM1.2	 (pin 7)
 motor R: P0.8=marcha atras PWM1.1 (pin 6) - P0.9= marcha adelante PWM1.1	 (pin 5)

 ----------------------SENSORES-----------------------

 ADC:
 P0.23=ADC0.0= Sensor Infrarrojo derecho		(pin 15)
 P0.24=ADC0.1= Sensor Infrarrojo izquierdo	(pin 16)

 ULTRASONICO:
 P0.4= capture timer2 (echo)  	(pin 38)
 P0.5= trigger					(pin 39)

 ------------------------UART2------------------------

 P0.10= TXD2
 P0.11= RXD2

 -----------------------DISPLAY-----------------------

 P2.0 al P2.6 = segmentos a,b,c,d,e,f,g respectivamente
 P2.7 = display 1
 p2.8 = display 2

 */

void adcConfig(void);
void clockConfig(void);
void ControlMotor(short, short, int, int);
void motorR(short);
void motorL(short);
void pwmConfig(void);
void retardo(unsigned int);
void timer0Config(void);
void timer1Config(void);
void timer2Config(void);
void ConfigTimer1y0();
void uartConfig(void);
void getAdcValue(void);
void adcControl(void);
void getUS(void);

void opModoSeguidor(void);
void opModoManual(void);
void opModoObstaculo(void);
int NumDisplay(unsigned int);

void SelectModoManual(void);
void SelectModoSeguidor(void);
void SelectModoObstaculo(void);

unsigned int FRONTERAIR;			//Frontera para los valores de los IR
unsigned int FRONTERAUS;			//Frontera para el valor del US

unsigned short IRR;		  //INFRARROJO CANAL 0
unsigned short IRL;		  //INFRARROJO CANAL 2

int contarDisplay; // si es 1 el display cunenta de 0 a 99, si no, muestra otro mensaje

int distReady;
int Distancia;
int rateR;
int rateL;

int modoManual;  // si es 1, se maneja por BT si es 0 siqgue la linea
int modoObstaculo;
int modoSeguidor;
int ordencero;
int iTimer2;

unsigned int Decenas;
unsigned int Unidades;
unsigned int Seg;
int T2int;
int main(void) {
	distReady = 0;
	Distancia = 0;
	iTimer2 = 0;
	rateR = 80;
	rateL = 75;
	IRL = 0;
	IRR = 0;

	Decenas = 0;
	Unidades = 0;
	Seg = 0;

	*FIO0DIR |= (1 << 6);
	*FIO0DIR |= (1 << 7);
	*FIO0DIR |= (1 << 8);
	*FIO0DIR |= (1 << 9);
	*FIO0CLR |= (1 << 6) | (1 << 7) | (1 << 8) | (1 << 9);

	// Configuraciones de los perifericos

	int tiempo = 30000;
	*FIO3DIR |= (3 << 25);
	for (int i = 0; i <= 10; i++) {
		*FIO0SET |= (1 << 22);
		*FIO3SET |= (3 << 25);
		retardo(tiempo);
		*FIO3CLR |= (1 << 25);
		retardo(tiempo);
		*FIO3SET |= (1 << 25);
		*FIO3CLR |= (1 << 26);
		retardo(tiempo);
		*FIO3SET |= (1 << 26);
		*FIO0CLR |= (1 << 22);
		retardo(tiempo);
		tiempo = tiempo - 3000;
	}
	*FIO0SET |= (1 << 22);
	*FIO3SET |= (3 << 25);

	ConfigTimer1y0();
	pwmConfig();
	adcConfig();
	timer2Config();
	uartConfig();

	FRONTERAIR = 700;
	contarDisplay = 0;
	SelectModoSeguidor();
	Decenas = 0;
	Unidades = 0;
	Seg = 0;
	Distancia = 50;
	SelectModoManual();
	while (1) {

		//===MODO: SEGUIDOR DE LINEA=====
		if (modoSeguidor == 1 && modoObstaculo == 0) {
			opModoSeguidor();
		}

		//===MODO: OBSTACULO=======
		if (modoObstaculo == 1 && modoSeguidor == 0) {
			opModoObstaculo();
		}

		//==== MODO MANUAL, ESTA EN EL UART2IRQ

	}
	return 0;
}

/*
 ================================================================
 ================================================================
 =============== FUNCIONES 	MODOS  DE OPERACION =================
 ================================================================
 ================================================================
 */

void opModoSeguidor(void) {
	static int Veces = 0;
	getUS();

	if (Distancia >= 22) {
		*FIO0SET |= (1 << 22);
		getAdcValue();
		adcControl();
		Veces = 0;

	} else {
		if (Veces >= 10) {
			ControlMotor(0, 0, 100, 100);
			*FIO0CLR |= (1 << 22);
			SelectModoObstaculo();
		}
		Veces++;
		return;
	}

	return;
}

void opModoObstaculo(void) {
	int comparador = 3;
	int orden = 0;
	int contador = 0;
	int lineFound = 0;
	while (lineFound == 0) {
		while (contador <= comparador) {
			switch (orden) {

			case 0:
				comparador = 110;
				ordencero = 0;
				//	orden = orden + 1;
				//contador = 0;
				ControlMotor(-1, 1, 65,70);		//Giro a la izquierda
				//retardo(500000);
				break;

			case 1:
				comparador = 10;
				ControlMotor(1, 1, 65,70);		//avanzo derecho
				break;
			case 2:
				comparador = 170;
				ControlMotor(1, 0, 65,70);		//giro a la derecha
				break;
			case 3:
				ordencero = 1;
				comparador = 100;
				ControlMotor(1, 1, 65,70);		//avanzo derecho
				break;
			case 4:

				comparador = 100;
				ControlMotor(0, 1, 65,70);		//giro a la derecha
				break;

			default:
				contador = 1000000;
				ControlMotor(0, 0, rateL, rateR);
				SelectModoManual();
				lineFound = 1;
				break;

			}

			retardo(1000);
			ControlMotor(0, 0, 20, 20);
			retardo(1000);
			if (ordencero == 1) {
				getAdcValue();

				if ((IRR < FRONTERAIR) || (IRL < FRONTERAIR)) {
					ControlMotor(0, 0, rateL, rateR);
					lineFound = 1;
					contador = 10100;
					SelectModoSeguidor();
					return;
				}
			}
			contador++;

		}
		orden = orden + 1;
		contador = 0;
	}

	modoManual = 1;
	modoObstaculo = 0;
	modoSeguidor = 0;
	return;

}

void SelectModoManual(void) {
	*FIO0SET |= (1 << 22);
	*FIO3SET |= (3 << 25);
	modoManual = 1;
	modoObstaculo = 0;
	modoSeguidor = 0;
	*FIO3CLR |= (1 << 26);
	return;

}
void SelectModoSeguidor(void) {
	*FIO0SET |= (1 << 22);
	*FIO3SET |= (3 << 25);
	modoManual = 0;
	modoObstaculo = 0;
	modoSeguidor = 1;
	*FIO3CLR |= (1 << 25);
	return;

}
void SelectModoObstaculo(void) {
	*FIO0SET |= (1 << 22);
	*FIO3SET |= (3 << 25);
	modoManual = 0;
	modoObstaculo = 1;
	modoSeguidor = 0;
	*FIO0CLR |= (1 << 22);
	return;

}

/*
 ================================================================
 ================================================================
 =============== FUNCIONNES/RUTINAS  AUXILIARES =================
 ================================================================
 ================================================================
 */

void ControlMotor(short ML, short MR, int PWML, int PWMR) {

	switch (ML) {		//P0.9 P0.8 pines para marcha adelante y marcha atras
	case -1:					//del motor izquierdo
		*FIO0CLR |= (1 << 9);	//caso marcha atras
		*FIO0SET |= (1 << 8);
		motorL(PWML);
		break;
	case 0:
		*FIO0CLR |= (1 << 8);
		*FIO0CLR |= (1 << 9);
		motorL(PWML);
		break;
	case 1:
		*FIO0CLR |= (1 << 8);
		*FIO0SET |= (1 << 9);
		motorL(PWML);
		break;
	default:
		break;
	}

	switch (MR) {		//P0.7 P0.6 pines para marcha adelante y marcha atras
	case -1:					//del motor derecho
		*FIO0CLR |= (1 << 7);	//caso marcha atras
		*FIO0SET |= (1 << 6);
		motorR(PWMR);
		break;
	case 0:
		*FIO0CLR |= (1 << 6);
		*FIO0CLR |= (1 << 7);
		motorR(PWMR);
		break;
	case 1:
		*FIO0CLR |= (1 << 6);
		*FIO0SET |= (1 << 7);
		motorR(PWMR);
		break;
	default:
		break;
	}

	return;
}

void motorR(short valorNuevo) {

	if (*PWM1MR1 > valorNuevo) {

		while (!((*PWM1MR1 - valorNuevo) == 0)) {

			*PWM1MR1 = *PWM1MR1 - 1;
			//retardo(300);
		}
	} else {
		while (!((valorNuevo - *PWM1MR1) == 0)) {

			*PWM1MR1 = *PWM1MR1 + 1;

		}
	}
}

void motorL(short valorNuevo) {

	if (*PWM1MR2 > valorNuevo) {

		while (!((*PWM1MR2 - valorNuevo) == 0)) {
			*PWM1MR2 = *PWM1MR2 - 1;

		}
	} else {
		while (!((valorNuevo - *PWM1MR2) == 0)) {
			*PWM1MR2 = *PWM1MR2 + 1;
		}
	}
}

void retardo(unsigned int time) {
	unsigned int i;
	for (i = 0; i < time; i++)
		; //lazo de demora
}

void getAdcValue(void) {

	*AD0CR &= ~0xFF;
	*AD0CR |= (1 << 0);
	*AD0CR |= (1 << 24); //START
	while (!(*AD0STAT & (1 << 0))) {
		;
	}
	IRR = (*AD0DR0 >> 4) & 0xFFF;

	*AD0CR &= ~0xFF;
	retardo(1000);
	*AD0CR |= (1 << 2);
	*AD0CR |= (1 << 24); //START
	while (!(*AD0STAT & (1 << 2))) {
		;
	}
	IRL = (*AD0DR2 >> 4) & 0xFFF;

	return;

}

void adcControl(void) {

	if ((IRL < FRONTERAIR) && (IRR < FRONTERAIR)) {	//Estan sobre la linea negra
		ControlMotor(0, 0, rateL, rateR);
		SelectModoManual();
		//ControlMotor(-1, -1, rateL, rateR);
		//CASO ESPECIAL
	} else if ((IRL < FRONTERAIR) && (IRR > FRONTERAIR)) {//Doblar a la derecha
		ControlMotor(-1, 1, 99, rateR);

	} else if ((IRL > FRONTERAIR) && (IRR < FRONTERAIR)) {//Doblar a la izquierda

		ControlMotor(1, -1, rateL, 99);

	} else if ((IRL > FRONTERAIR) && (IRR > FRONTERAIR)) {		//Seguir derecho

		ControlMotor(1, 1, rateL, rateR);

	}
	retardo(8000);
			ControlMotor(0, 0, 20, 20);
			retardo(100);
	/*if (Distancia <= 50) {
		retardo(8000);
		ControlMotor(0, 0, 20, 20);
		retardo(100);
	} else {
		retardo(10000);
		ControlMotor(0, 0, 20, 20);
		retardo(100);

	}*/
	return;
}

void getUS(void) {
	int C = 0;
	int Promedio = 0;

	for (int j = 0; j < 3; j++) {
		distReady = 0;
		T2int = 1;
		*FIO0SET |= (1 << 5);
		retardo(100);
		iTimer2 = 0;
		*T2TCR |= (1 << 1);
		*T2TCR &= ~(1 << 1);
		*FIO0CLR |= (1 << 5);

		while (distReady == 0 && C <= 100000) {
			C += 1;
		}
		if (distReady == 0) {
			Distancia = Promedio;
		}
		T2int = 0;

		Promedio = (Promedio + Distancia) / 2;
	}
	Distancia = Promedio;
	return;
}

int NumDisplay(unsigned int num) {
	switch (num) {
	case 0:
		return 0b0111111;
		break;
	case 1:
		return 0b0000110;
		break;
	case 2:
		return 0b1011011;
		break;
	case 3:
		return 0b1001111;
		break;
	case 4:
		return 0b1100110;
		break;
	case 5:
		return 0b1101101;
		break;
	case 6:
		return 0b1111101;
		break;
	case 7:
		return 0b0000111;
		break;
	case 8:
		return 0b1111111;
		break;
	case 9:
		return 0b1100111;
		break;
	case 10:
		return 0b0101100;
		break;
	case 11:
		return 0b0011010;
		break;
	default:
		return 0;
		break;

	}
}
/*
 ================================================================
 ================================================================
 =============== INTERRUPCIONES DE LOS PERIFERICOS ==============
 ================================================================
 ================================================================
 */

void UART2_IRQHandler(void) {
	char dato;
	dato = *U2RBR;
	if (modoManual == 1) {
		switch (dato) {
		case 'a':
			ControlMotor(1, 1, rateL, rateR);	// adelante
			break;
		case 'b':
			ControlMotor(-1, -1, rateL, rateR);	// atras
			break;
		case 'c':
			ControlMotor(1, -1, rateL, rateR);	// adelante-derecha
			break;
		case 'd':
			ControlMotor(-1, 1, rateL, rateR);	//adelante-izquierda
			break;
		case 'e':
			ControlMotor(0, -1, rateL, rateR);	// atras-derecha
			break;
		case 'f':
			ControlMotor(-1, 0, rateL, rateR);		// atras-izquieda
			break;
		case 'g':
			SelectModoSeguidor();
			Seg = 0;
			break;
		case 'h': 			// apagar luces
			break;
		case 'p': 			// apagar motores
			ControlMotor(0, 0, 0, 0);

			break;

		default:
			break;

		}
	}
	if (dato == 'h') {
		SelectModoManual();
		Seg = 0;
	}

	//*FIO3CLR = (1 << 25) | (1 << 26);
	//*FIO0CLR |= (1 << 22);
	return;
}

void TIMER0_IRQHandler(void) {
	static int i = 0;
	if (i == 0) {
		*FIO2CLR |= (1 << 8);
		*FIO2SET |= NumDisplay(Unidades) & 0b1111111;
		*FIO2CLR |= (~NumDisplay(Unidades)) & 0b1111111;
		*FIO2SET |= (1 << 7);
		i = 1;
	} else if (i == 1) {
		*FIO2CLR |= (1 << 7);
		*FIO2SET |= NumDisplay(Decenas) & 0b1111111;
		*FIO2CLR |= (~NumDisplay(Decenas)) & 0b1111111;
		*FIO2SET |= (1 << 8);
		i = 0;
	}
	*T0IR |= 1;
}

void TIMER1_IRQHandler(void) {
	Seg++;
	if (Seg >= 99) {
		modoManual = 0;
		modoObstaculo = 0;
		modoSeguidor = 0;
		*FIO3CLR |= (3 << 25);
		*FIO0CLR |= (1 << 22);
		Unidades = 10;
		Decenas = 11;
		ControlMotor(0, 0, 90, 90);

	} else {
		Decenas = Seg / 10;
		Unidades = Seg - (10 * Decenas);
	}
	*T1IR |= (1 << 0);
	return;
}

/*  Leo el valor del timer en el flanco de subida y luego el de bajada, luego la resta de los tiempos me da
 el valor enviado por el sensor.
 */

void TIMER2_IRQHandler(void) {
	static int FlancoSub = 0; // tiempo en el que se produce el flanco de subida
	static int FlancoBaj = 0; // tiempo en el que se produce el flanco de bajada

	if ((*T2IR & (1 << 4))) {
		if (T2int == 1) {
			if (iTimer2 == 0) {
				FlancoSub = *T2CR0;
				iTimer2 = 1;
			} else {
				FlancoBaj = *T2CR0;

				Distancia = (FlancoBaj - FlancoSub) / 58; // el valor de distancia esta en cm

				iTimer2 = 0;
				distReady = 1;
				if (Distancia < 0) {
					Distancia = 30;
					distReady = 0;
				}

			}
		}

		*T2IR |= (1 << 4);
	}

	return;
}

/*
 ================================================================
 ================================================================
 =========== CONFIGURACION DE LOS PERIFERICOS ===================
 ================================================================
 ================================================================
 */

// Configuracion del ADC, se usan  P0.23 (Sensor R) y P0.24 (Sensor L)
void adcConfig() {

	*PCONP |= (1 << 12); 				//Enciendo el clock del periferico ADC
	*AD0CR |= (1 << 0) | (1 << 2); //se habilitan el canal 0 y canal 1 para conversion
	*AD0CR |= (1 << 21); 				//Esta habilitado el conversor A/D

	*PCLKSEL0 &= ~(3 << 24);
	*PCLKSEL0 |= (1 << 24);
	*AD0CR &= ~(255 << 8); 			//no divide
	*AD0CR &= ~(1 << 16); 				//convierte permanentemente

	//3. Pins: Enable ADCO pins through PINSEL registers. Select the pin modes for the port pins with ADCO functions through the PINMODE registers (Section 8.5).
	*PINMODE1 |= (1 << 15);	//pin P0.23 no hay resistencias de pullup ni pulldown
	*PINMODE1 |= (1 << 19);	//pin P0.25 no hay resistencias de pullup ni pulldown

	*PINSEL1 |= (1 << 14); 			//pin P0.23 como AD0.0
	*PINSEL1 |= (1 << 18);				//pin P0.24 como AD0.1

	*AD0INTEN = 0; //Cuando finaliza la conversion en el canal 0 o el canal 1, se produce una interrupcion

	*ISER0 &= ~(1 << 22); //ADC Interrupt Enable.
}

// Configuracion del PWM
void pwmConfig(void) {

	*PCONP |= (1 << 6);
	*PCLKSEL0 |= (1 << 12);
	*PINSEL3 |= (2 << 4) | (2 << 8);		//Pines P1.18=PWM1.1 P1.20=PWM1.2
	*PINMODE3 |= (2 << 4) | (2 << 8);
	*PWM1PR |= 2;
	*PWM1MR0 = 100;
	*PWM1MR1 = 0;
	*PWM1MR2 = 0;
	*PWM1MCR |= 2;					//Se resetea el timer cuando TC=MR0
	*PWM1PCR |= (1 << 9) | (1 << 10);//se habilita PWM1.1 y PWM1.2 por defecto se trabaja de un solo flanco
	*PWM1TCR = 1;
}

// Configuracion del UART2, se lo configura a 9600baudios, como clock cpu=100Mhz, lo dividimos en 4 y luego DLL=163 y DLM=0.
//Pines P0.10 y P0.11 como TXD2 y RXD2 respectivamente
void uartConfig(void) {

	*PCONP |= (1 << 24);
	*PCLKSEL1 &= ~(3 << 16);
	*PCLKSEL1 |= (1 << 17);				//divido el CLK en 2

	*U2LCR |= 3;

	*U2LCR |= (1 << 7);
	*U2DLL = 13;						// da 9615 baudios
	*U2DLM = 0;
	*U2LCR &= ~(1 << 7);

	*PINSEL0 |= (1 << 20) | (1 << 22);
	*U2IER = 1;

	*IPR1 &= ~(0x1F << 27);
	*IPR1 |= (1 << 27);

	*ISER0 |= (1 << 7);
}

void ConfigTimer1y0(void) {

	*FIO2DIR |= 511; // P2.0 a P2.6 como salida para el display
					 // y P2.7 y 8 para el multiplexeo
	//*************Configuracion TMR0 y TMR1
	// TMR0 para multiplexear
	// TMR1 para captura
	*PCONP |= (1 << 1) | (1 << 2);//Activo el clock de los periferico TMR0 y el del TMR1

	*PCLKSEL0 |= (1 << 2) | (1 << 4);// No divido la frecuencia del clok para ningun timer

	//*PINSEL3 |= (3 << 4); //configuro el pin p1.18 para que funcione en modo captura

	*T0MCR |= 3; // el TMRO debe producir una interrupcion y resetearse cada vez que se pr
				 //produce el match
	*T1MCR |= (1 << 0) | (1 << 1);// El TMR1 se resetea cada vez que se produce un match0 y se interrumpe cuando hay un match1

	*T0MR0 = 20000;	// el TMR0 lo uso para el multiplexeo de los Displays

	*T1PR = 2000000;
	*T1MR0 = 1;	// el TMR1 lo uso para saber cuando hay que resetear el TC
				// y para la captura

	*T1CCR |= 0; 		//Cuando suceda una captura, se produce una interrupcion
						// la captura se produce en el flanco de bajada del PIN
	*IPR0 &= ~(0x1F << 11);
	*IPR0 &= ~(0x1F << 19);
	*IPR0 |= (2 << 19);
	*IPR0 |= (2 << 11);

	*ISER0 |= (1 << 1) | (1 << 2); // Activo la interrupcion por timer 0 y por timer 1
	*T0TCR |= 1; // cuando el bit cero esta en uno,el timer y el prescaler estan
				 //listos para contar
	*T1TCR |= 1; // Activo el contador para el timer1

	return;
}
/* Configuracion de Timer2: se configura en modo Capture y un match.
 para que por el capture0 se tome la señal enviada del Sensor de Ultrasonido
 Se necesita que el TC cuente cada 1us (10Mhz).
 Como el reloj del cpu esta a 100MHz (T=10nS), se divide por 100 el clock del CPU. Primero por 2 en el PCLKSEL1
 y por 5 en el Prescaler del timer.


 La captura es por flanco de subida yy bajada, se toma el valor del flanco de subida y luego el de bajada
 el valor dado por el sensor es proporcional a la diferencia entre estos dos tiempos


 El Match se produce a los 100 ms para enviar el trigger al sensor

 el reseteo del  timer se produce despues de leer el flanco de bajada del echo
 */
void timer2Config(void) {
	*FIO0DIR |= (1 << 22);		//led 22 salida
	*FIO0SET |= (1 << 22);
	*PINSEL0 &= ~(3 << 10);
	*FIO0DIR |= (1 << 5);		//led 22 salida
	*FIO0CLR |= (1 << 5);
	*FIO0DIR &= ~(1 << 4);
	*PCONP |= (1 << 22);

	*PCLKSEL1 &= ~(1 << 13); // divido por 2 , ahora hay 50Mhz de clock
	*T2PR = 0;              // Lla frecuencia del  contador es de 10MHz

	*PINMODE0 |= (3 << 8);		// P0.4 con resistencia de pulldown
	*PINSEL0 |= (3 << 8);		//P0.4 COMO CAP2.0

	*T2CCR |= 1 | (1 << 1) | (1 << 2); // La captura se produce por flanco de subida y de bajada. Ademas se activa la interrupcion

	*T2MCR = 0;					// El match solo produce una interrupcion
	*IPR0 &= ~(0x1F << 27);
	*T2IR |= 0xFF;
	*ISER0 |= (1 << 3);

	*T2TCR |= 1; //arranca el conteo

	return;
}

