#include "Registros.h"

void adcConfig(void);
void uartConfig(void);
void clockConfig(void);
void pwmConfig(void);
void enviar(char);
void ControlMotor(short, short);
int NumDisplay(unsigned int);



volatile int contarDisplay=0;





unsigned int FRONTERAIR;			//Frontera para los valores de los IR
unsigned int FRONTERAUS;			//Frontera para el valor del US
unsigned int quehago;				//valor que ira al pwmOperation

int modoManual;
int contarDisplay ;

unsigned int Decenas = 0;				//para el timer
unsigned int Unidades = 0;

int main(void) {
	*FIO0DIR |= (1 << 22);
	*FIO0CLR |= (1 << 22);
	*FIO2CLR |= (1 << 0);
	*FIO2CLR |= (1 << 1);

	*FIO2SET |= (1 << 0);
	*FIO2SET |= (1 << 1);
	*FIO0SET |= (1 << 22);
	clockConfig();
	adcConfig();
	uartConfig();

	FRONTERAIR = 700;

	while (1) {
	}
	return 0;
}

void timerConfig(void){
	*PCONP |= (3<<2);			//perifericos Timer0 y timer1 habilitados.
	*PCLKSEL0 |= (5<<2);		//clk del timer0/1 como CCLK
	//timer0
	*T0MCR|=(3<<0);				//Se interrumpe  
	*T0MR0=99;
	*T0PR|=2000000;
	//timer1
	*T1MCR |= 3;				//al darse el match se interrumpe actualiza displays y reinicia el conteo
	*T1PR|=4;					//prescaler en 4
	*T1MR0 = 20000;
	*ISER0|= (3<<1);			//se habilita interrupcion por TIMER0/1
}



void uartConfig(void)
{
	*PCONP |= 1<<24;					//Se habilita el perifeco UART2
	*PCLKSEL1 &= ~(3<<16);				//El CLK del UART2 es PCLK/8
	*U2LCR |= 3;						//Palabra de 8bits
	*U2LCR |= (1 << 7);			//DLAB=1 para modificar los registros DLL y DLM
	*U2DLL = 163;						//DLL=163
	*U2DLM = 0;							//DLM=0
	*U2LCR &= ~(1 << 7);//DLAB=0 para poder cargar o recibir valores al THR y RBR
	*PINSEL0 |= (5 << 20);//Pines P0.10 y P0.11 como TXD2 y RXD2 respectivamente
	*U2IER = 1;						//Interrupcion en casa de recibir un dato
	*ISER0 |= (1 << 7);						//se habilitan interrupciones
}

void adcConfig() {
//1. Power: In the PCONP register (Table 46), set the PCADC bit.
	*PCONP |= (1 << 12); 				//Enciendo el clock del periferico ADC
	*AD0CR |= (3 << 0);		//se habilitan el canal 0 y canal 1 para conversion
	*AD0CR |= (1 << 21); 				//Esta habilitado el conversor A/D

//2. Clock: In the PCLKSEL0 register (Table 40), select PCLK_ADC.
	//   To scale the clock for the ADC, see bits CLKDIV in Table 532.
	*PCLKSEL0 |= (3 << 24);		//divide en 8 el clock del ADC clkADC=12,5MHz
	*AD0CR &= ~(255 << 8); 			//no divide
	*AD0CR |= 1 << 16; 				//convierte permanentemente

//3. Pins: Enable ADCO pins through PINSEL registers. Select the pin modes for the port pins with ADCO functions through the PINMODE registers (Section 8.5).
	*PINMODE1 |= (1 << 15);	//pin P0.23 no hay resistencias de pullup ni pulldown
	*PINMODE1 |= (1 << 17);	//pin P0.24 no hay resistencias de pullup ni pulldown
	*PINMODE1 |= (1 << 19);	//pin P0.25 no hay resistencias de pullup ni pulldown
	*PINSEL1 |= (1 << 14); 			//pin P0.23 como AD0.0
	*PINSEL1 |= (1 << 16);				//pin P0.24 como AD0.1
	*PINSEL1 |= (1 << 18);				//pin P0.24 como AD0.2

//4. Interrupts: To enable interrupts in the ADC, see Table 536. Interrupts are enabled in the NVIC using the appropriate Interrupt Set Enable register.
	*AD0INTEN = 3; //Cuando finaliza la conversion en el canal 0 o el canal 1, se produce una interrupcion

	*ISER0 |= (1 << 22); //ADC Interrupt Enable.
}

void clockConfig(void) {
	*SCS = 32;
	while ((*SCS & (1 << 16)) == 0)
		;//El bit 6 es el OSCSTAT el cual indica con un 1 en él si el main oscillator esta listo para usarse
	*CCLKCFG = 0x3;	//Se puede dividir al CCLK de 1 a 255 dependiendo del valor en eeste registro, si el PLL0 esta running entonces este
					//no puede valer 1, pues hay que tener en cuenta que este clk va al CPU el cual soporta hasta 120MHz como maximo
	*PCLKSEL0 = 0x0;			//para los perifericos el clk esta dividido en 4
	*PCLKSEL1 = 0x0;
	*CLKSRCSEL = 0x1;//Se selecciona el oscilador principal a usar en este caso se selecciona para PLL0
	*PLL0CFG = 0x50063;			//Se colocan los valores de (M - 1) y (N - 1)
	*PLL0FEED = 0xAA;						//Secuencia de FEED correcta
	*PLL0FEED = 0x55;
	*PLL0CON = 0x01; /* PLLO Enable */
	*PLL0FEED = 0xAA;						//secuencia de FEED correcta
	*PLL0FEED = 0x55;
	while (!(*PLL0STAT & (1 << 26)))
		;//Hasta que el bit PLOCK0 no este en uno el PLL0 no se encuentra en la frecuencia solicitada
	*PLL0CON = 0x01;						//Se habilita el PLL0
	*PLL0FEED = 0xAA;						//Secuencia de FEED correcta
	*PLL0FEED = 0x55;
	while (!(*PLL0STAT & ((1 << 25) | (1 << 24))))
		;// cuando estos bits esten en 1 quiere decir que el PLL0 esta conectado como fuente de reloj al CPU

}

/*	si el valor que se pasa es :
 -1: Marcha atras
 0: Parar
 1: Marcha adelante
 */
// Supongo  que MR esta controlado por el P0.0 (MARCHA) Y P0.1(CONTRAMARCHA)
//// Supongo  que MR esta controlado por el P0.7 (MARCHA) Y P0.6 (CONTRAMARCHA)
void ControlMotor(short ML, short MR) {
	switch (ML) {
	case -1:
		*FIO2CLR |= (1 << 0);		// marcha, apago p0.0 y prendo p0.1
		*FIO2SET |= (1 << 1);
		break;
	case 0:
		*FIO2CLR |= (1 << 0);
		*FIO2CLR |= (1 << 1);
		break;
	case 1:
		*FIO2CLR |= (1 << 1);
		*FIO2SET |= (1 << 0);
		break;
	default:
		break;
	}

	switch (MR) {
	case -1:
		*FIO0CLR |= (1 << 7);
		*FIO0SET |= (1 << 6);
		break;
	case 0:
		*FIO0CLR |= (1 << 7);
		*FIO0CLR |= (1 << 6);
		break;
	case 1:
		*FIO0CLR |= (1 << 6);
		*FIO0SET |= (1 << 7);
		break;
	default:
		break;
	}

	return;
}

int NumDisplay(unsigned int num) {
	switch (num) {
	case 0:
		return 0b0111111;
		break;
	case 1:
		return 0b0000110;
		break;
	case 2:
		return 0b1011011;
		break;
	case 3:
		return 0b1001111;
		break;
	case 4:
		return 0b1100110;
		break;
	case 5:
		return 0b1101101;
		break;
	case 6:
		return 0b1111101;
		break;
	case 7:
		return 0b0000111;
		break;
	case 8:
		return 0b1111111;
		break;
	case 9:
		return 0b1100111;
		break;
	default:
		return 0;
		break;

	}
}

void enviar(char c) {
	while ((*U2LSR & (1 << 5)) == 0)
		; // espero que el Unthr este vacio
	*U2THR = c;
	return;
}

void ADC_IRQHandler(void) {
	//SENSORES INFRARROJOS
	static unsigned short IRR = 0;		  //INFRARROJO CANAL 0
	static unsigned short IRL = 0;		  //INFRARROJO CANAL 1
	static unsigned short US = 0;		  //Ulstrasonico CANAL 2

	IRR = (*AD0DR0 >> 4) & 0xFFF;
	IRL = (*AD0DR1 >> 4) & 0xFFF;
	US = (*AD0DR2 >> 4) & 0xFFF;

	if ((IRR < FRONTERAIR) && (IRL < FRONTERAIR)) {	//Estan sobre la linea negra
		ControlMotor(1, 1);
		//CASO ESPECIAL
	} else if ((IRR > FRONTERAIR) && (IRL < FRONTERAIR)) {		  //

		ControlMotor(0, 1);
	} else if ((IRR < FRONTERAIR) && (IRL > FRONTERAIR)) {

		ControlMotor(1, 0);

	} else if ((IRR > FRONTERAIR) && (IRL > FRONTERAIR)) {

		ControlMotor(1, 1);

	}

	
	if (US > FRONTERAUS){

		*T0TCR=1;			//empieza el TIMER0 a contar
	}
	return;
}

void UART2_IRQHandler(void) {
	char dato;
	dato = *U2RBR;
	if (modoManual == 1) {
		switch (dato) {
		case 'a':
			ControlMotor(1, 1);	// adelante
			break;
		case 'b':
			ControlMotor(-1, -1);	// atras
			break;
		case 'c':
			ControlMotor(1, -1);	// adelante-derecha
			break;
		case 'd':
			ControlMotor(-1, 1);	//adelante-izquierda
			break;
		case 'e':
			ControlMotor(0, -1);	// atras-derecha
			break;
		case 'f':
			ControlMotor(-1, 0);		// atras-izquieda
			break;
		case 'g':
			modoManual = 0;		// Apagar Modo manual
			contarDisplay = 0;
			break;
		case 'h': 			// apagar luces
			break;

		default:
			break;

		}
	}
	return;
}

void TIMER0_IRQHandler(void) {
	modoManual = 1;
	*T0IR |= (1 << 4);
	return;
}

void TIMER1_IRQHandler(void) {
	static int i = 0;
	static int cont = 0;

	if (contarDisplay == 1) {
		Decenas = *T1CR0 / 10;
		Unidades = *T1CR0 - (10 * Decenas);

		if (i == 0) {
			*FIO2CLR |= (1 << 8);
			*FIO2SET |= NumDisplay(Unidades) & 0b1111111;
			*FIO2CLR |= (~NumDisplay(Unidades)) & 0b1111111;
			*FIO2SET |= (1 << 7);
			*FIO0SET |= (1 << 22);
			i = 1;
		} else if (i == 1) {
			*FIO2CLR |= (1 << 7);
			*FIO2SET |= NumDisplay(Decenas) & 0b1111111;
			*FIO2CLR |= (~NumDisplay(Decenas)) & 0b1111111;
			*FIO2SET |= (1 << 8);
			*FIO0CLR |= (1 << 22);
			i = 0;
		}
	} else {
		cont+=1;
		if (i == 0) {
			*FIO2CLR |= (1 << 8);
			if (cont <= 100) {
				*FIO2SET |= 0b0111001 & 0b1111111;
				*FIO2CLR |= (~0b0111001) & 0b1111111;
			} else {
				*FIO2SET |= 0b1000000 & 0b1111111;
				*FIO2CLR |= (~0b1000000) & 0b1111111;
			}

			*FIO2SET |= (1 << 7);
			*FIO0SET |= (1 << 22);
			i = 1;
		} else if (i == 1) {
			*FIO2CLR |= (1 << 7);
			if (cont <= 100) {
				*FIO2SET |= 0b0001111 & 0b1111111;
				*FIO2CLR |= (~0b0001111) & 0b1111111;
			} else {
				*FIO2SET |= 0b1000000 & 0b1111111;
				*FIO2CLR |= (~0b1000000) & 0b1111111;

			}

			*FIO2SET |= (1 << 8);
			*FIO0CLR |= (1 << 22);
			i = 0;
		}
		if(cont==200){
			cont=0;
		}

	}

	*T1IR |= 1;
}

