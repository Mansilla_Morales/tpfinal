#define AddrFIO0SET 0x2009C018
#define AddrFIO2SET 0x2009C058
#define AddrFIO0CLR 0x2009C01C
#define AddrFIO2CLR 0x2009C05C
#define AddrFIO0PIN 0x2009C014
#define AddrFIO2PIN 0x2009C054
#define AddrFIO3SET 0x2009C078
#define AddrFIO3CLR 0x2009C07C
#define AddrFIO3DIR 0x2009C060


// REGISTROS DE CONFIGURACION DE PINES Y PERIFERICOS
#define AddrFIO0DIR 0x2009C000
#define AddrFIO2DIR 0x2009C040
#define AddrPINMODE0  0x4002C040
#define AddrPINMODE1 0x4002C044
#define AddrPINMODE3 0x4002C04C
#define AddrPINMODE4 0x4002C050
#define AddrPINSEL0  0x4002C000
#define AddrPINSEL1  0x4002C004
#define AddrPINSEL3  0x4002C00C
#define AddrPINSEL4 0x4002C010
#define AddrPCONP 		0x400FC0C4
#define AddrPCLKSEL0 	0x400FC1A8
#define AddrPCLKSEL1    0x400FC1AC
#define AddrISER0 		0xE000E100 //pagina 79
#define AddrIPR0        0xE000E400
#define AddrIPR1 	 0xE000E404
#define AddrICER0 0xE000E180
#define AddrICER1 0xE000E184

/// REGISTROS DE ADC
#define AddrAD0CR 		0x40034000
#define AddrAD0INTEN 	0x4003400C
#define AddrAD0DR0 		0x40034010
#define AddrAD0DR1 		0x40034014
#define AddrAD0DR2		0x40034018
#define AddrAD0STAT 	0x40034030

// REGISTROS DE TIMER 0 Y TIMER 1 Y TIMER 2
#define AddrT0EMR 0x4000403C
#define AddrT0MCR 0x40004014
#define AddrT0CCR 0x40004028
#define AddrT0MR0 0x40004018
#define AddrT0TCR 0x40004004
#define AddrT0IR  0x40004000
#define AddrT0PR  0x4000400C

#define AddrT1MCR 0x40008014
#define AddrT1CCR 0x40008028
#define AddrT0MR1 0x4000401C
#define AddrT1MR0 0x40008018
#define AddrT1TCR 0x40008004
#define AddrT1IR  0x40008000
#define AddrT1PR  0x4000800C
#define AddrT1CR0 0x4000802C

#define AddrT2IR  0x40090000
#define AddrT2TCR  0x40090004
#define AddrT2PR	0x4009000C
#define AddrT2MCR  	0x40090014
#define AddrT2MR0	0x40090018
#define AddrT2CCR  0x40090028
#define AddrT2CR0 0x4009002C


//REGISTROS UART
#define AddrU2LCR	0x4009800C
#define AddrU2DLL	0x40098000
#define AddrU2DLM	0x40098004
#define AddrU2IER	0x40098004
#define AddrU2THR	0x40098000
#define AddrU2LSR	0x40098014
#define AddrU2RBR	0x40098000

//REGISTROS PWM
#define AddrPWM1TCR 	0x40018004 	//PMW TIMER CONTROL REGISTER
#define AddrPWM1MCR 	0x40018014 	//PWM MATCH CONTROL REGISTER
#define AddrPWM1TC  	0x40018008  	//PWM TIME COUNTER
#define AddrPWM1PCR 	0x4001804C 	//PWM CONTROL REGISTER
#define AddrPWM1MR0 	0x40018018 	//PWM MATCH REGISTER 0
#define AddrPWM1MR1 	0x4001801C 	//PWM MATCH REGISTER 1
#define AddrPWM1MR2 	0x40018020 	//PWM MATCH REGISTER 2
#define AddrPWM1CTCR	0x40018070 //PWM COUNT CONTROL REGISTER EN 0 PARA UTILIZAR EL MODO TIMER
#define AddrPWM1PC 0x40018010
#define AddrPWM1PR 0x4001800C


//REGISTROS PLL0
#define AddrSCS 		0x400FC1A0
#define AddrCCLKCFG 	0x400FC104
#define AddrCLKSRCSEL 	0x400FC10C
#define AddrPLL0CFG 	0x400FC084
#define AddrPLL0FEED 	0x400FC08C
#define AddrPLL0CON 	0x400FC080
#define AddrPLL0STAT 	0x400FC088




unsigned int volatile *const FIO0SET =(unsigned int *) AddrFIO0SET ;
unsigned int volatile *const FIO2SET =(unsigned int *) AddrFIO2SET ;
unsigned int volatile *const FIO3SET =(unsigned int *) AddrFIO3SET ;
unsigned int volatile *const FIO0CLR =(unsigned int *) AddrFIO0CLR ;
unsigned int volatile *const FIO2CLR =(unsigned int *) AddrFIO2CLR ;
unsigned int volatile *const FIO3CLR =(unsigned int *) AddrFIO3CLR ;
unsigned int volatile *const FIO0PIN =(unsigned int *) AddrFIO0PIN ;
unsigned int volatile *const FIO2PIN =(unsigned int *) AddrFIO2PIN ;


unsigned int volatile *const FIO0DIR =(unsigned int *) AddrFIO0DIR  ;
unsigned int volatile *const FIO2DIR =(unsigned int *) AddrFIO2DIR  ;
unsigned int volatile *const FIO3DIR =(unsigned int *) AddrFIO3DIR  ;
unsigned int volatile * const PINMODE0 = (unsigned int*) AddrPINMODE0;
unsigned int volatile *const PINMODE1 =(unsigned int *)  AddrPINMODE1  ;
unsigned int volatile *const PINMODE3 =(unsigned int *)  AddrPINMODE3  ;
unsigned int volatile *const PINMODE4 =(unsigned int *) AddrPINMODE4 ;
unsigned int volatile *const PINSEL0  =(unsigned int *)  AddrPINSEL0   ;
unsigned int volatile *const PINSEL1  =(unsigned int *)  AddrPINSEL1   ;
unsigned int volatile *const PINSEL3  =(unsigned int *)  AddrPINSEL3   ;
unsigned int volatile *const PINSEL4 =(unsigned int *) AddrPINSEL4 ;
unsigned int volatile *const PCONP 	  =(unsigned int *)  AddrPCONP 	 ;
unsigned int volatile *const PCLKSEL0 =(unsigned int *)  AddrPCLKSEL0  ;
unsigned int volatile *const PCLKSEL1 =(unsigned int *)  AddrPCLKSEL1  ;
unsigned int volatile *const ISER0 	  =(unsigned int *)  AddrISER0 	 ;
unsigned int volatile *const IPR0  	  =(unsigned int *)  AddrIPR0 	 ;
unsigned int volatile *const IPR1  	  =(unsigned int *)  AddrIPR1 	 ;
unsigned int volatile *const ICER1   =(unsigned int *)  AddrICER1;
unsigned int volatile *const ICER0   =(unsigned int *)  AddrICER0;


unsigned int volatile *const AD0CR 	=(unsigned int *) AddrAD0CR 	  ;
unsigned int volatile *const AD0INTEN=(unsigned int *) AddrAD0INTEN  ;
unsigned int volatile *const AD0DR0 =(unsigned int *) AddrAD0DR0 	  ;
unsigned int volatile *const AD0DR1 =(unsigned int *) AddrAD0DR1 	  ;
unsigned int volatile *const AD0DR2 =(unsigned int *) AddrAD0DR2 	  ;
unsigned int volatile *const AD0STAT =(unsigned int *) AddrAD0STAT  ;


unsigned int volatile *const T0EMR=(unsigned int *)  AddrT0EMR ;
unsigned int volatile *const T0MCR=(unsigned int *)  AddrT0MCR ;
unsigned int volatile *const T0CCR=(unsigned int *)  AddrT0CCR ;
unsigned int volatile *const T0MR0=(unsigned int *)  AddrT0MR0 ;
unsigned int volatile *const T0TCR=(unsigned int *)  AddrT0TCR ;
unsigned int volatile *const T0IR =(unsigned int *)  AddrT0IR  ;
unsigned int volatile *const T0PR =(unsigned int *)  AddrT0PR  ;


unsigned int volatile *const T1MCR =(unsigned int *)  AddrT1MCR  ;
unsigned int volatile *const T1CCR =(unsigned int *)  AddrT1CCR  ;
unsigned int volatile *const T0MR1 =(unsigned int *)  AddrT0MR1  ;
unsigned int volatile *const T1MR0 =(unsigned int *)  AddrT1MR0  ;
unsigned int volatile *const T1TCR =(unsigned int *)  AddrT1TCR  ;
unsigned int volatile *const T1IR  =(unsigned int *)  AddrT1IR   ;
unsigned int volatile *const T1PR  =(unsigned int *)  AddrT1PR   ;
unsigned int volatile *const T1CR0 =(unsigned int *)  AddrT1CR0  ;

unsigned int volatile * const T2IR = (unsigned int*) AddrT2IR;
unsigned int volatile * const T2TCR = (unsigned int*) AddrT2TCR;
unsigned int volatile * const T2PR = (unsigned int*) AddrT2PR;
unsigned int volatile * const T2CCR = (unsigned int*) AddrT2CCR;
unsigned int volatile * const T2CR0 = (unsigned int*) AddrT2CR0;
unsigned int volatile * const T2MR0 = (unsigned int*) AddrT2MR0;
unsigned int volatile * const T2MCR = (unsigned int*) AddrT2MCR;



unsigned int volatile *const U2LCR =(unsigned int *) AddrU2LCR ;
unsigned int volatile *const U2DLL =(unsigned int *) AddrU2DLL ;
unsigned int volatile *const U2DLM =(unsigned int *) AddrU2DLM ;
unsigned int volatile *const U2IER =(unsigned int *) AddrU2IER ;
unsigned int volatile *const U2THR =(unsigned int *) AddrU2THR ;
unsigned int volatile *const U2LSR =(unsigned int *) AddrU2LSR ;
unsigned int volatile *const U2RBR =(unsigned int *) AddrU2RBR ;


unsigned int volatile *const PWM1TCR  =(unsigned int *) AddrPWM1TCR   ;
unsigned int volatile *const PWM1MCR  =(unsigned int *) AddrPWM1MCR   ;
unsigned int volatile *const PWM1TC   =(unsigned int *) AddrPWM1TC    ;
unsigned int volatile *const PWM1PCR  =(unsigned int *) AddrPWM1PCR   ;
unsigned int volatile *const PWM1MR0  =(unsigned int *) AddrPWM1MR0   ;
unsigned int volatile *const PWM1MR1  =(unsigned int *) AddrPWM1MR1   ;
unsigned int volatile *const PWM1MR2  =(unsigned int *) AddrPWM1MR2   ;
unsigned int volatile *const PWM1CTCR =(unsigned int *) AddrPWM1CTCR  ;
unsigned int volatile *const PWM1PC =(unsigned int *) AddrPWM1PC ;
unsigned int volatile *const PWM1PR =(unsigned int *) AddrPWM1PR ;

unsigned int volatile *const SCS 		 =(unsigned int *)AddrSCS 		;
unsigned int volatile *const CCLKCFG 	 =(unsigned int *)AddrCCLKCFG 	;
unsigned int volatile *const CLKSRCSEL =(unsigned int *)AddrCLKSRCSEL;
unsigned int volatile *const PLL0CFG 	 =(unsigned int *)AddrPLL0CFG 	;
unsigned int volatile *const PLL0FEED  =(unsigned int *)AddrPLL0FEED ;
unsigned int volatile *const PLL0CON 	 =(unsigned int *)AddrPLL0CON 	;
unsigned int volatile *const PLL0STAT  =(unsigned int *)AddrPLL0STAT ;


